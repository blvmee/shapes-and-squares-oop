package com.company;

public class SquareCounter {

    static Shape[] shapes = new Shape[4];

    public static void main(String[] args) {
        shapes[0] = new Rectangle();
        shapes[1] = new Triangle();
        shapes[2] = new Circle();
        shapes[3] = new Trapezoid();
        for(Shape s : shapes) {
            System.out.println(s.getSquare());
        }
    }

}
