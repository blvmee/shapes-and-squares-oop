package com.company;

public class Trapezoid extends Shape {

    public Trapezoid() {

    }

    int sideA = 3;
    int sideB = 5;
    int height = 7;

    @Override
    double getSquare() {
        return ((double) (sideA + sideB) / 2 * height);
    }
}
