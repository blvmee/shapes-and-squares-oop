package com.company;

public class Triangle extends Shape {

    int sideA = 3;
    int sideB = 4;
    int sideC = 5;
    double halfPeri = (double) (sideA + sideB + sideC)/2;

    public Triangle() {

    }

    double square = Math.sqrt(halfPeri*(halfPeri-sideA)*(halfPeri-sideB)*(halfPeri-sideC));

    @Override
    double getSquare() {
        return square;
    }
}
