package com.company;

public class Circle extends Shape{

    int radius = 5;

    public Circle() {

    }

    @Override
    public double getSquare(){
        return 2*3.14*radius*radius;
    }

}
